namespace comp5002_10008721_assessment2
{
    
    class Subjects
    {
        private string CourseCode;
        private string CourseName;
        private string Teacher;
        public string courseCode
        {
            get
            {
                return CourseCode;
            }
            set
            {
                CourseCode = value;
            }
        }
        public string courseName
        {
            get
            {
                return CourseName;
            }
            set
            {
                courseName = value;
            }
        }     
        public string teacher
        {
            get
            {
                return Teacher;
            }
            set
            {
                Teacher = value;
            }
        }    
        public Subjects(string teacher, string courseCode, string courseName)
        {
            Teacher = teacher;
            CourseName = courseName;
            CourseCode = courseCode;
        }
    }
}  
namespace comp5002_10008721_assessment2
{
    
    class Person
    {
        private string Name;
        private string UserName;
        private int Id;
        public string name
        {
            get
            {
                return Name;
            }
            set
            {
                Name = value;
            }
        } 
        public string userName
        {
            get
            {
                return UserName;
            }
            set
            {
                UserName = value;
            }
        } 
        public int id
        {
            get
            {
                return Id;
            }
            set
            {
                Id = value;
            }
        }   
        public Person(string name, int id, string userName)
        {
            Name = name;
            Id = id;
            UserName = userName;
        }     
    }  
}





﻿using System;
using System.Collections.Generic;

namespace comp5002_10008721_assessment2
{

    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            Console.Clear();
            List<Student> studentList = new List<Student>();
            studentList.Add(new Student("Kyrin", 10008732, "KS19"));
            studentList.Add(new Student("Louise", 10008782, "LK39"));
            studentList.Add(new Student("Matt", 10008726, "MS39"));
            studentList.Add(new Student("Liam", 10008742, "LS11"));
            List<Teacher> teacherList = new List<Teacher>();

            teacherList.Add(new Teacher("Jeffrey Kranenburg", 10008726, "Jk11"));
            teacherList.Add(new Teacher("Stefan Stasiewicz", 10008727, "SS3"));
            teacherList.Add(new Teacher("Murray Foote", 10008728, "MF12"));
            teacherList.Add(new Teacher("Ray Scott", 10008729, "RS13"));
            teacherList.Add(new Teacher("John Achilles", 10008929, "JA73"));

            List<Subjects> subjectsList = new List<Subjects>();
            subjectsList.Add(new Subjects("Jeffrey Kranenburg", "COMP5002", "Introduction To Programing"));
            subjectsList.Add(new Subjects("Ray Scott", "INFT5001", "Professional Skills"));
            subjectsList.Add(new Subjects("Murray Foote", "COMP5008", "Software Packages"));
            subjectsList.Add(new Subjects("Stefan Stasiewicz", "COMP5004", "IT Infrastructure"));

            foreach (var item in teacherList)
            {
            Console.WriteLine(Teacher.WhatAmITeaching(subjectsList, item.name));
            }
            foreach (var item in studentList)
            { 
            Console.WriteLine(Student.ListAllMySubjects(subjectsList, item.name));
            }

            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }       
    }
}





using System.Collections.Generic;

namespace comp5002_10008721_assessment2
{
    
    class Student : Person
    {
        public Student(string name, int id, string userName) : base (name,  id, userName)
        {
        }
        public static string ListAllMySubjects(List<Subjects> subjectsList, string name ) 
        {
            var output = "";
            foreach (var x in subjectsList)
            {
                output += $"{name} is studying {x.courseCode} - {x.courseName}\n";       
            }
            return output;         
        } 
    }
}




